package com.example.contadorvidas.ui.main;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.contadorvidas.R;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private ConstraintLayout main;
    private Button btn2poisonplus;
    private Button btn1poisonplus;
    private Button btn1poisonless;
    private Button btn2poisonless;
    private ImageButton btn2lifeless;
    private ImageButton btn2lifeplus;
    private ImageButton btn1lifeplus;
    private ImageButton btn1lifeless;
    private ImageButton lifetwootone;
    private ImageButton lifeonetotwo;
    private TextView counter1;
    private TextView counter2;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;
    private View view;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);

        main = (ConstraintLayout) view.findViewById(R.id.main);
        btn2poisonplus = (Button) view.findViewById(R.id.btn2poisonplus);
        btn1poisonplus = (Button) view.findViewById(R.id.btn1poisonplus);
        btn1poisonless = (Button) view.findViewById(R.id.btn1poisonless);
        btn2poisonless = (Button) view.findViewById(R.id.btn2poisonless);
        btn2lifeless = (ImageButton) view.findViewById(R.id.btn2lifeless);
        btn2lifeplus = (ImageButton) view.findViewById(R.id.btn2lifeplus);
        btn1lifeplus = (ImageButton) view.findViewById(R.id.btn1lifeplus);
        btn1lifeless = (ImageButton) view.findViewById(R.id.btn1lifeless);
        lifetwootone = (ImageButton) view.findViewById(R.id.lifetwootone);
        lifeonetotwo = (ImageButton) view.findViewById(R.id.lifeonetotwo);
        counter1 = (TextView) view.findViewById(R.id.counter1);
        counter2 = (TextView) view.findViewById(R.id.counter2);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.lifeonetotwo:
                        life1--;
                        life2++;
                        break;

                    case R.id.lifetwootone:
                        life1++;
                        life2--;
                        break;

                    case R.id.btn1lifeplus:
                        life1++;
                        break;

                    case R.id.btn1lifeless:
                        life1--;
                        break;

                    case R.id.btn1poisonplus:
                        poison1++;
                        break;

                    case R.id.btn1poisonless:
                        poison1--;
                        break;

                    case R.id.btn2lifeplus:
                        life2++;
                        break;

                    case R.id.btn2lifeless:
                        life2--;
                        break;

                    case R.id.btn2poisonplus:
                        poison2++;
                        break;

                    case R.id.btn2poisonless:
                        poison2--;
                        break;
                }
                updateViews();
            }
        };

        reset();

        lifeonetotwo.setOnClickListener(listener);
        lifetwootone.setOnClickListener(listener);
        btn1poisonplus.setOnClickListener(listener);
        btn1poisonless.setOnClickListener(listener);
        btn1lifeless.setOnClickListener(listener);
        btn1lifeplus.setOnClickListener(listener);
        btn2poisonplus.setOnClickListener(listener);
        btn2poisonless.setOnClickListener(listener);
        btn2lifeless.setOnClickListener(listener);
        btn2lifeplus.setOnClickListener(listener);
        counter1.setOnClickListener(listener);
        counter2.setOnClickListener(listener);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.reset) {
            reset();
            Snackbar.make(view, "New Game", Snackbar.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void reset() {
        poison1 = 0;
        poison2 = 0;
        life1 = 20;
        life2 = 20;

        updateViews();
    }

    private void updateViews() {
        counter1.setText(String.format("%d/%d",life1,poison1));
        counter2.setText(String.format("%d/%d",life2,poison2));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}